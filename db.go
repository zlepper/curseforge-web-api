package curseforgewebapi

import (
	"context"
	"github.com/pkg/errors"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
)

var (
	ErrNoSuchEntity = errors.New("Entity was not found in database")
)

type AppengineDatastore struct {
}

func (ad *AppengineDatastore) SaveProject(ctx context.Context, project *Project) error {
	key := datastore.NewKey(ctx, DATASTORE_PROJECT_KIND, "", project.ProjectId, nil)

	_, err := datastore.Put(ctx, key, project)
	if err != nil {
		log.Errorf(ctx, "Failed to insert project into datastore: %v", err)
	}
	return err
}

func (ad *AppengineDatastore) SaveProjectFile(ctx context.Context, projectId int64, file *ProjectFile) error {
	projectKey := datastore.NewKey(ctx, DATASTORE_PROJECT_KIND, "", projectId, nil)

	projectFileKey := datastore.NewKey(ctx, DATASTORE_PROJECT_FILE_KIND, "", file.ProjectFileId, projectKey)

	_, err := datastore.Put(ctx, projectFileKey, file)
	if err != nil {
		log.Errorf(ctx, "Failed to insert project file into datastore: %v", err)
	}
	return err
}

const DATASTORE_READ_SIZE_LIMIT = 20

func (ad *AppengineDatastore) GetAllProjects(ctx context.Context, startCursor string) ([]Project, string, error) {
	cursor, err := datastore.DecodeCursor(startCursor)
	if err != nil {
		log.Errorf(ctx, "Failed to decode start cursor: %v", err)
		return nil, "", err
	}

	query := datastore.NewQuery(DATASTORE_PROJECT_KIND).Start(cursor)

	t := query.Run(ctx)

	cursorDone := false
	projects := make([]Project, 0)
	for i := 0; i < DATASTORE_READ_SIZE_LIMIT; i++ {
		var project Project
		_, err := t.Next(&project)
		if err == datastore.Done {
			cursorDone = true
			break
		}
		if err != nil {
			log.Errorf(ctx, "Failed to fetch next project: %v", err)
			return nil, "", err
		}
		projects = append(projects, project)
	}

	cursor, err = t.Cursor()
	if err != nil {
		log.Errorf(ctx, "Failed to get updated cursor: %v", err)
		return nil, "", err
	}

	nextCursor := ""
	if !cursorDone {
		nextCursor = cursor.String()
	}

	return projects, nextCursor, nil
}

func (ad *AppengineDatastore) GetProject(ctx context.Context, projectId int64) (Project, error) {
	key := datastore.NewKey(ctx, DATASTORE_PROJECT_KIND, "", projectId, nil)

	var project Project
	err := datastore.Get(ctx, key, &project)
	if err != nil {
		if err == datastore.ErrNoSuchEntity {
			return project, ErrNoSuchEntity
		}
		log.Errorf(ctx, "Failed to get project '%d' from datastore: %v", projectId, err)
		return project, err
	}

	return project, nil
}

func (ad *AppengineDatastore) GetProjectFiles(ctx context.Context, projectId int64, startCursor string) ([]ProjectFile, string, error) {
	cursor, err := datastore.DecodeCursor(startCursor)
	if err != nil {
		log.Errorf(ctx, "Failed to decode start cursor: %v", err)
		return nil, "", err
	}

	projectKey := datastore.NewKey(ctx, DATASTORE_PROJECT_KIND, "", projectId, nil)

	t := datastore.NewQuery(DATASTORE_PROJECT_FILE_KIND).Ancestor(projectKey).Start(cursor).Run(ctx)

	cursorDone := false
	files := make([]ProjectFile, 0)
	for i := 0; i < DATASTORE_READ_SIZE_LIMIT; i++ {
		var file ProjectFile
		_, err := t.Next(&file)
		if err == datastore.Done {
			cursorDone = true
			break
		}
		if err == datastore.ErrNoSuchEntity {
			return nil, "", ErrNoSuchEntity
		}
		if err != nil {
			log.Errorf(ctx, "Failed to fetch next project file from datastore: %v", err)
			return nil, "", err
		}
		files = append(files, file)
	}

	cursor, err = t.Cursor()
	if err != nil {
		log.Errorf(ctx, "Failed to get updated cursor: %v", err)
		return nil, "", err
	}

	nextCursor := ""
	if !cursorDone {
		nextCursor = cursor.String()
	}

	return files, nextCursor, nil
}

func (ad *AppengineDatastore) GetProjectFile(ctx context.Context, projectId, projectFileId int64) (ProjectFile, error) {
	projectKey := datastore.NewKey(ctx, DATASTORE_PROJECT_KIND, "", projectId, nil)

	projectFileKey := datastore.NewKey(ctx, DATASTORE_PROJECT_FILE_KIND, "", projectFileId, projectKey)

	var projectFile ProjectFile
	err := datastore.Get(ctx, projectFileKey, &projectFile)
	if err != nil {
		if err == datastore.ErrNoSuchEntity {
			return projectFile, ErrNoSuchEntity
		}

		log.Errorf(ctx, "Failed to get project '%d' file '%d' from datastore: %v", projectId, projectFileId, err)
		return projectFile, err
	}

	return projectFile, nil
}
