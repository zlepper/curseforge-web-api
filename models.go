package curseforgewebapi

import (
	"time"
)

type ErrorResponse struct {
	Error string `json:"error"`
}

type Project struct {
	Type string
	Name string
	Link string
	ProjectId int64
	Created time.Time
	LastUpdated time.Time
	TotalDownloads int64
}

type MinecraftVersion struct {
	Version string
}

type ProjectFile struct {
	Filename string
	ProjectFileId int64
	UploadDate time.Time
	DownloadCount int64
	Md5 string
	Changelog string `datastore:",noindex"`
	ProjectId int64
	SupportedMinecraftVersions []MinecraftVersion
}