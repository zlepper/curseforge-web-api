package curseforgewebapi

import (
	"context"
	"github.com/pkg/errors"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/taskqueue"
	"net/url"
	"strconv"
)

type AppengineTaskQueue struct {
}

func (atr *AppengineTaskQueue) enqueue(ctx context.Context, task *taskqueue.Task) error {
	_, err := taskqueue.Add(ctx, task, "")
	if err != nil {
		log.Errorf(ctx, "Failed to enqueue project page crawl task: %v", err)
		return errors.Wrap(err, "Appengine enqueue failed")
	}

	return nil
}

func (atq *AppengineTaskQueue) EnqueueProjectPageCrawl(ctx context.Context, link string) error {

	task := taskqueue.NewPOSTTask("/api/tasks/crawl-project-page", url.Values{
		"link": {link},
	})

	return atq.enqueue(ctx, task)

}

func (atq *AppengineTaskQueue) EnqueueProjectFileListCrawl(ctx context.Context, page int, link string, projectId int64) error {
	task := taskqueue.NewPOSTTask("/api/tasks/crawl-file-list", url.Values{
		"link":      {link},
		"page":      {strconv.Itoa(page)},
		"projectId": {strconv.FormatInt(projectId, 10)},
	})

	return atq.enqueue(ctx, task)
}

func (atq *AppengineTaskQueue) EnqueueSingleFileCrawl(ctx context.Context, link string, projectId int64) error {

	task := taskqueue.NewPOSTTask("/api/tasks/crawl-single-file", url.Values{
		"link":      {link},
		"projectId": {strconv.FormatInt(projectId, 10)},
	})

	return atq.enqueue(ctx, task)
}
