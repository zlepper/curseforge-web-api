package curseforgewebapi

import (
	"context"
	"github.com/labstack/echo"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/taskqueue"
	"net/http"
	"net/url"
)

func bindTaskQueueHandlers(e *echo.Group) {
	tg := e.Group("/tasks")

	queue := &AppengineTaskQueue{}
	storage := &AppengineDatastore{}

	crawler := &curseForgeCrawler{
		queue:   queue,
		storage: storage,
	}

	ts := taskServer{
		crawler: crawler,
	}

	tg.GET("/daily", ts.startDailyTasks)
	tg.POST("/crawl-project-list", ts.crawlProjectList)
	tg.POST("/crawl-project-page", ts.crawlProjectPage)
	tg.POST("/crawl-file-list", ts.crawlFileList)
	tg.POST("/crawl-single-file", ts.crawlSingleFile)
}

type iCurseForgeCrawler interface {
	// Crawl the front project list page
	CrawlProjectListPage(ctx context.Context, link string, page int) error

	// Crawls a specific project page
	CrawlProjectPage(ctx context.Context, link string) error

	// Crawls a single page of the file list
	CrawlFileList(ctx context.Context, page int, link string, projectId int64) error

	// Crawls a single project file
	CrawlSingleFile(ctx context.Context, link string, projectId int64) error
}

type taskServer struct {
	crawler iCurseForgeCrawler
}

func (taskServer) startDailyTasks(c echo.Context) error {
	ctx := appengine.NewContext(c.Request())

	mcModsTask := taskqueue.NewPOSTTask("/api/tasks/crawl-project-list", url.Values{
		"page": {"1"},
		"link": {"/mc-mods"},
	})
	modpacksTask := taskqueue.NewPOSTTask("/api/tasks/crawl-project-list", url.Values{
		"page": {"1"},
		"link": {"/modpacks"},
	})

	_, err := taskqueue.AddMulti(ctx, []*taskqueue.Task{mcModsTask, modpacksTask}, "")
	if err != nil {
		log.Errorf(ctx, "Failed to add initial crawl tasks to the task queue: %v", err)
		return c.JSON(http.StatusInternalServerError, ErrorResponse{err.Error()})
	}
	return c.NoContent(http.StatusOK)
}

func (ts *taskServer) crawlProjectList(c echo.Context) error {
	ctx := appengine.NewContext(c.Request())

	var params struct {
		Page int    `form:"page"`
		Link string `form:"link"`
	}
	err := c.Bind(&params)
	if err != nil {
		log.Errorf(ctx, "Failed to get form params: %v", err)
		return c.JSON(http.StatusBadRequest, ErrorResponse{err.Error()})
	}

	err = ts.crawler.CrawlProjectListPage(ctx, params.Link, params.Page)
	if err != nil {
		log.Errorf(ctx, "Failed to crawl project list page. Params: %+v, err: %v", params, err)
		return c.JSON(http.StatusInternalServerError, ErrorResponse{err.Error()})
	}

	return c.NoContent(http.StatusOK)
}

func (ts *taskServer) crawlProjectPage(c echo.Context) error {
	ctx := appengine.NewContext(c.Request())

	var params struct {
		Link string `form:"link"`
	}
	err := c.Bind(&params)
	if err != nil {
		log.Errorf(ctx, "Failed to get params: %v", err)
		return c.JSON(http.StatusBadRequest, ErrorResponse{err.Error()})
	}

	err = ts.crawler.CrawlProjectPage(ctx, params.Link)
	if err != nil {
		log.Errorf(ctx, "Failed to crawl project page. Params: %+v, err: %v", err)
		return c.JSON(http.StatusInternalServerError, ErrorResponse{err.Error()})
	}

	return c.NoContent(http.StatusOK)
}

func (ts *taskServer) crawlFileList(c echo.Context) error {
	ctx := appengine.NewContext(c.Request())

	var params struct {
		Page      int    `form:"page"`
		Link      string `form:"link"`
		ProjectId int64  `form:"projectId"`
	}

	err := c.Bind(&params)
	if err != nil {
		log.Errorf(ctx, "Failed to get params: %v", err)
		return c.JSON(http.StatusBadRequest, ErrorResponse{err.Error()})
	}

	err = ts.crawler.CrawlFileList(ctx, params.Page, params.Link, params.ProjectId)
	if err != nil {
		log.Errorf(ctx, "Failed to crawl file list. Params: %+v, err: %v", params, err)
		return c.JSON(http.StatusInternalServerError, ErrorResponse{err.Error()})
	}

	return c.NoContent(http.StatusOK)
}

func (ts *taskServer) crawlSingleFile(c echo.Context) error {
	ctx := appengine.NewContext(c.Request())

	var params struct {
		Link      string `form:"link"`
		ProjectId int64  `form:"projectId"`
	}
	err := c.Bind(&params)
	if err != nil {
		log.Errorf(ctx, "Failed to get params: %v", err.Error())
		return c.JSON(http.StatusBadRequest, ErrorResponse{err.Error()})
	}

	err = ts.crawler.CrawlSingleFile(ctx, params.Link, params.ProjectId)
	if err != nil {
		log.Errorf(ctx, "Failed to crawl single file: %v", err)
		return c.JSON(http.StatusInternalServerError, ErrorResponse{err.Error()})
	}

	return c.NoContent(http.StatusOK)

}
