package curseforgewebapi

const (
	CURSE_FORGE_BASE_URL = "https://minecraft.curseforge.com"

	DATASTORE_PROJECT_KIND = "project"
	DATASTORE_PROJECT_FILE_KIND = "projectFile"
)
