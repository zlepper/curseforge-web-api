package curseforgewebapi

import (
	"context"
	"fmt"
	"github.com/labstack/echo"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"net/http"
	"strconv"
)

func BindApi(e *echo.Group) {
	bindTaskQueueHandlers(e)

	db := &AppengineDatastore{}
	cache := &AppengineCache{}

	apiServer := &ApiServer{
		db:    db,
		cache: cache,
	}

	cache.Register(AllProjectsResponse{})
	cache.Register(SingleProjectResponse{})
	cache.Register(ProjectFilesResponse{})
	cache.Register(SingleProjectFileResponse{})

	e.GET("/projects", apiServer.GetAllProjects)
	e.GET("/projects/:projectId", apiServer.GetProject)
	e.GET("/projects/:projectId/files", apiServer.GetProjectFiles)
	e.GET("/projects/:projectId/files/:projectFileId", apiServer.GetProjectFile)

}

type apiDatabase interface {
	GetAllProjects(ctx context.Context, startCursor string) (projects []Project, nextCursor string, err error)
	GetProject(ctx context.Context, projectId int64) (Project, error)
	GetProjectFiles(ctx context.Context, projectId int64, startCursor string) (files []ProjectFile, nextCursor string, err error)
	GetProjectFile(ctx context.Context, projectId, projectFileId int64) (ProjectFile, error)
}

type cache interface {
	Add(ctx context.Context, key string, object interface{}) error
	Get(ctx context.Context, key string, out interface{}) error
	GetOrCreate(ctx context.Context, key string, out interface{}, creator func(item interface{}) error) error
	Register(sample interface{})
}

type ApiServer struct {
	db    apiDatabase
	cache cache
}

type AllProjectsResponse struct {
	// The actual projects
	Projects []Project `json:"projects"`
	// The relative link to the next page
	// If the key is not there, then there are no more results
	Next string `json:"next,omitempty"`
}

func (a *ApiServer) GetAllProjects(c echo.Context) error {
	ctx := appengine.NewContext(c.Request())

	startCursor := c.QueryParam("cursor")

	cacheKey := fmt.Sprintf("%s", startCursor)

	var response AllProjectsResponse
	err := a.cache.GetOrCreate(ctx, cacheKey, &response, func(responseItem interface{}) error {
		response := responseItem.(*AllProjectsResponse)

		projects, nextCursor, err := a.db.GetAllProjects(ctx, startCursor)
		if err != nil {
			log.Errorf(ctx, "Failed to get all projects from the database: %v", err)
			return err
		}

		nextUrl := ""
		if nextCursor != "" {
			nextUrl = fmt.Sprintf("/api/projects?cursor=%s", nextCursor)
		}

		response.Next = nextUrl
		response.Projects = projects

		return nil
	})
	if err != nil {
		log.Errorf(ctx, "Failed to pull data through cache: %v", err)
		return c.JSON(http.StatusInternalServerError, ErrorResponse{err.Error()})
	}
	if err != nil {
		return c.JSON(http.StatusInternalServerError, ErrorResponse{err.Error()})
	}

	return c.JSON(http.StatusOK, response)
}

type SingleProjectResponse struct {
	Project Project `json:"project"`
}

func (a *ApiServer) GetProject(c echo.Context) error {
	ctx := appengine.NewContext(c.Request())

	projectIdParam := c.Param("projectId")

	projectId, err := strconv.ParseInt(projectIdParam, 10, 64)
	if err != nil {
		log.Errorf(ctx, "Failed to convert project id param into int: %v", err)
		return c.JSON(http.StatusBadRequest, ErrorResponse{err.Error()})
	}

	cacheKey := fmt.Sprintf("%d", projectId)

	var response SingleProjectResponse
	err = a.cache.GetOrCreate(ctx, cacheKey, &response, func(item interface{}) error {
		response := item.(*SingleProjectResponse)
		project, err := a.db.GetProject(ctx, projectId)
		if err != nil {
			if err == ErrNoSuchEntity {
				log.Infof(ctx, "Project with id '%d' was not found", projectId)
				return err
			}

			log.Errorf(ctx, "Failed to get project from database: %v", err)
			return err
		}

		response.Project = project
		return nil
	})
	if err == ErrNoSuchEntity {
		return c.JSON(http.StatusNotFound, ErrorResponse{err.Error()})
	}
	if err != nil {
		return c.JSON(http.StatusInternalServerError, ErrorResponse{err.Error()})
	}

	return c.JSON(http.StatusOK, response)
}

type ProjectFilesResponse struct {
	Files []ProjectFile `json:"files"`
	Next  string        `json:"next,omitempty"`
}

func (a *ApiServer) GetProjectFiles(c echo.Context) error {
	ctx := appengine.NewContext(c.Request())

	projectIdParam := c.Param("projectId")

	projectId, err := strconv.ParseInt(projectIdParam, 10, 64)
	if err != nil {
		log.Errorf(ctx, "Failed to convert project id param into int: %v", err)
		return c.JSON(http.StatusBadRequest, ErrorResponse{err.Error()})
	}

	startCursor := c.QueryParam("cursor")

	cacheKey := fmt.Sprintf("%d-%s", projectId, startCursor)

	var response ProjectFilesResponse
	err = a.cache.GetOrCreate(ctx, cacheKey, &response, func(item interface{}) error {
		response := item.(*ProjectFilesResponse)

		files, nextCursor, err := a.db.GetProjectFiles(ctx, projectId, startCursor)
		if err != nil {
			if err == ErrNoSuchEntity {
				log.Infof(ctx, "Project with id '%d' was not found", projectId)
				return err
			}

			log.Errorf(ctx, "Failed to get files for project: %v", err)
			return err
		}

		nextUrl := ""
		if nextCursor != "" {
			nextUrl = fmt.Sprintf("/api/projects/%d/files?cursor=%s", projectId, nextCursor)
		}

		response.Next = nextUrl
		response.Files = files

		return nil
	})
	if err == ErrNoSuchEntity {
		return c.JSON(http.StatusNotFound, ErrorResponse{err.Error()})
	}
	if err != nil {
		return c.JSON(http.StatusInternalServerError, ErrorResponse{err.Error()})
	}

	return c.JSON(http.StatusOK, response)
}

type SingleProjectFileResponse struct {
	File ProjectFile `json:"file"`
}

func (a *ApiServer) GetProjectFile(c echo.Context) error {
	ctx := appengine.NewContext(c.Request())

	projectIdParam := c.Param("projectId")

	projectId, err := strconv.ParseInt(projectIdParam, 10, 64)
	if err != nil {
		log.Errorf(ctx, "Failed to convert project id param into int: %v", err)
		return c.JSON(http.StatusBadRequest, ErrorResponse{err.Error()})
	}

	projectFileIdParam := c.Param("projectFileId")

	projectFileId, err := strconv.ParseInt(projectFileIdParam, 10, 64)
	if err != nil {
		log.Errorf(ctx, "Failed to convert project file id param into int: %v", err)
		return c.JSON(http.StatusBadRequest, ErrorResponse{err.Error()})
	}

	cacheKey := fmt.Sprintf("%d-%d", projectId, projectFileId)

	var response SingleProjectFileResponse
	err = a.cache.GetOrCreate(ctx, cacheKey, &response, func(item interface{}) error {
		response := item.(*SingleProjectFileResponse)

		file, err := a.db.GetProjectFile(ctx, projectId, projectFileId)
		if err != nil {
			if err == ErrNoSuchEntity {
				log.Infof(ctx, "project '%d' or project file '%d' do not exist", projectId, projectFileId)
				return err
			}

			log.Errorf(ctx, "Failed to get project file: %v", err)
			return err
		}

		response.File = file
		return nil
	})
	if err == ErrNoSuchEntity {
		return c.JSON(http.StatusNotFound, ErrorResponse{err.Error()})
	}
	if err != nil {
		return c.JSON(http.StatusInternalServerError, ErrorResponse{err.Error()})
	}

	return c.JSON(http.StatusOK, response)
}
