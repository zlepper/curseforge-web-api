package appengine

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"net/http"
	"gitlab.com/zlepper/curseforge-web-api"
)

func init() {

	e := echo.New()
	e.Pre(middleware.RemoveTrailingSlash())

	// note: we don't need to provide the middleware or static handlers, that's taken care of by the platform
	// app engine has it's own "main" wrapper - we just need to hook echo into the default handler
	http.Handle("/", e)

	apiGroup := e.Group("api")

	curseforgewebapi.BindApi(apiGroup)

}
