package curseforgewebapi

import (
	"context"
	"encoding/gob"
	"errors"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/memcache"
)

type AppengineCache struct {
}

func (ac *AppengineCache) GetOrCreate(ctx context.Context, key string, out interface{}, creator func(item interface{}) error) error {

	err := ac.Get(ctx, key, out)
	if err == ErrCacheMiss {
		err = creator(out)
		if err != nil {
			log.Errorf(ctx, "Creating items for cache failed: %v", err)
			return err
		}
		err = ac.Add(ctx, key, out)
		if err != nil {
			log.Errorf(ctx, "Adding item to cache failed: %v", err)
			return err
		}
		log.Infof(ctx, "Result was not in cache")
	} else {
		if err != nil {
			log.Errorf(ctx, "Cache error: %v", err)
			return err
		}
		log.Infof(ctx, "Got result from cache")
	}

	return nil
}

var (
	ErrCacheMiss = errors.New("cache miss")
)

func (ac *AppengineCache) Add(ctx context.Context, key string, object interface{}) error {
	err := memcache.Gob.Add(ctx, &memcache.Item{
		Key:    key,
		Object: object,
	})

	if err == memcache.ErrNotStored {
		return nil
	}

	return err
}

func (ac *AppengineCache) Get(ctx context.Context, key string, out interface{}) error {
	_, err := memcache.Gob.Get(ctx, key, out)

	if err == memcache.ErrCacheMiss {
		return ErrCacheMiss
	}

	return err
}

func (ac *AppengineCache) Register(sample interface{}) {
	gob.Register(sample)
}
