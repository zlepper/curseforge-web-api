package curseforgewebapi

import (
	"context"
	"fmt"
	"github.com/zlepper/yahc"
	_ "github.com/zlepper/yahc-encoding-html-bindings"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type curseStorage interface {
	// Saves the project to the underlying database
	SaveProject(ctx context.Context, project *Project) error

	// Saves the project file to the underlying database
	SaveProjectFile(ctx context.Context, projectId int64, projectFile *ProjectFile) error
}

type taskQueue interface {
	EnqueueProjectPageCrawl(ctx context.Context, link string) error
	EnqueueProjectFileListCrawl(ctx context.Context, page int, link string, projectId int64) error
	EnqueueSingleFileCrawl(ctx context.Context, link string, projectId int64) error
}

type curseForgeCrawler struct {
	storage curseStorage
	queue   taskQueue
}

func (cfc *curseForgeCrawler) get(ctx context.Context, out interface{}, linkFormat string, args ...interface{}) error {
	client := yahc.NewClient(urlfetch.Client(ctx))

	return client.Get(CURSE_FORGE_BASE_URL+fmt.Sprintf(linkFormat, args...), yahc.NoOptions, out)
}

func (cfc *curseForgeCrawler) CrawlProjectListPage(ctx context.Context, link string, page int) error {
	var response struct {
		Projects []struct {
			Name        string `css:".info.name .name-wrapper"`
			LastUpdated int64  `css:".info.stats .e-update-date abbr" extract:"attr" attr:"data-epoch"`
			Link        string `css:".info.name .name-wrapper a" extract:"attr" attr:"href"`
		} `css:".project-list-item"`
	}

	err := cfc.get(ctx, &response, "%s?page=%d", link, page)
	if err != nil {
		log.Errorf(ctx, "Failed to fetch curse project list page. Link '%s', page '%d': %v", link, page, err)
		return err
	}

	for _, project := range response.Projects {
		// TODO Check if the project changed since last time, in which case
		// TODO it will need to be re-indexed
		err = cfc.queue.EnqueueProjectPageCrawl(ctx, project.Link)
		if err != nil {
			log.Errorf(ctx, "Failed to enqueue project '%+v': %v", project, err)
			return err
		}
	}

	// TODO Enqueue next page if needed

	return nil
}

func (cfc *curseForgeCrawler) parseCurseForgeNumber(number string) (int64, error) {
	return strconv.ParseInt(strings.NewReplacer(".", "", ",", "").Replace(number), 10, 64)
}

func (cfc *curseForgeCrawler) CrawlProjectPage(ctx context.Context, link string) error {
	var response struct {
		Name             string `css:".project-details-container .project-title"`
		Link             string `css:".project-details-container .project-title a" extract:"attr" attr:"href"`
		Type             string `css:".project-details-container .RootGameCategory"`
		ProjectId        int64  `css:".project-details li:nth-child(1) .info-data"`
		Created          int64  `css:".project-details li:nth-child(2) .info-data abbr" extract:"attr" attr:"data-epoch"`
		LastReleasedFile int64  `css:".project-details li:nth-child(3) .info-data abbr" extract:"attr" attr:"data-epoch"`
		TotalDownloads   string `css:".project-details li:nth-child(4) .info-data"`
		FilesLink        string `css:".e-header-nav .e-menu li:nth-child(3) a" extract:"attr" attr:"href" default:""`
	}

	err := cfc.get(ctx, &response, link)
	if err != nil {
		log.Errorf(ctx, "Failed to fetch project page: %v", err)
		return err
	}

	downloads, err := cfc.parseCurseForgeNumber(response.TotalDownloads)
	if err != nil {
		log.Errorf(ctx, "Failed to convert download number '%s' to int64: %v", response.TotalDownloads, err)
		return err
	}

	project := &Project{
		Type:           strings.TrimSpace(response.Type),
		Link:           response.Link,
		ProjectId:      response.ProjectId,
		Created:        time.Unix(response.Created, 0),
		LastUpdated:    time.Unix(response.LastReleasedFile, 0),
		Name:           strings.TrimSpace(response.Name),
		TotalDownloads: downloads,
	}

	err = cfc.storage.SaveProject(ctx, project)
	if err != nil {
		log.Errorf(ctx, "Failed to save project to database: %v", err)
		return err
	}

	if response.FilesLink != "" {
		err = cfc.queue.EnqueueProjectFileListCrawl(ctx, 1, response.FilesLink, response.ProjectId)
		if err != nil {
			log.Errorf(ctx, "Failed to enqueue project file list crawl: %v", err)
			return err
		}
	}

	return nil
}

func (cfc *curseForgeCrawler) CrawlFileList(ctx context.Context, page int, link string, projectId int64) error {
	var response struct {
		Files []struct {
			Link string `css:".project-file-name-container a:nth-child(1)" extract:"attr" attr:"href"`
		} `css:".project-file-list-item"`
	}

	err := cfc.get(ctx, &response, "%s?page=%d", link, page)
	if err != nil {
		log.Errorf(ctx, "Failed to get project file list: %v", err)
		return err
	}

	for _, file := range response.Files {
		// TODO Check if file has already been crawled previously
		err = cfc.queue.EnqueueSingleFileCrawl(ctx, file.Link, projectId)
		if err != nil {
			log.Errorf(ctx, "Failed to enqueue single file crawl: %v", err)
			return err
		}
	}

	// TODO Crawl the remaining pages

	return nil
}

var (
	lastIdRegex = regexp.MustCompile("(\\d+)$")
)

func (cfc *curseForgeCrawler) getProjectFileId(link string) (int64, error) {
	projectFileIdS := lastIdRegex.FindString(link)
	return strconv.ParseInt(projectFileIdS, 10, 64)
}

func (cfc *curseForgeCrawler) CrawlSingleFile(ctx context.Context, link string, projectId int64) error {
	projectFileId, err := cfc.getProjectFileId(link)
	if err != nil {
		log.Errorf(ctx, "Failed to convert project file id '%s' to an int: %v", link, err)
		return err
	}

	var response struct {
		Filename            string `css:".details-info li:nth-child(1) .info-data"`
		UploadDate          int64  `css:".details-info li:nth-child(3) .info-data abbr" extract:"attr" attr:"data-epoch"`
		DownloadCount       string `css:".details-info li:nth-child(5) .info-data"`
		Md5                 string `css:".details-info li:nth-child(6) .info-data"`
		SupportedMcVersions []struct {
			Version string `css:"*"`
		} `css:".details-content .details-versions ul:nth-child(2) li"`
		Changelog string `css:".details-content .details-changelog .logbox"`
	}

	err = cfc.get(ctx, &response, link)
	if err != nil {
		log.Errorf(ctx, "Failed to fetch single file page: %v", err)
		return err
	}

	downloads, err := cfc.parseCurseForgeNumber(response.DownloadCount)
	if err != nil {
		log.Errorf(ctx, "Failed to parse download count '%s': %v", response.DownloadCount, err)
		return err
	}

	mcVersions := make([]MinecraftVersion, len(response.SupportedMcVersions))
	for index, v := range response.SupportedMcVersions {
		mcVersions[index] = MinecraftVersion{Version: strings.TrimSpace(v.Version)}
	}

	projectFile := &ProjectFile{
		Filename:                   strings.TrimSpace(response.Filename),
		UploadDate:                 time.Unix(response.UploadDate, 0),
		DownloadCount:              downloads,
		Md5:                        strings.TrimSpace(response.Md5),
		Changelog:                  strings.TrimSpace(response.Changelog),
		SupportedMinecraftVersions: mcVersions,
		ProjectId:                  projectId,
		ProjectFileId:              projectFileId,
	}

	err = cfc.storage.SaveProjectFile(ctx, projectId, projectFile)
	if err != nil {
		log.Errorf(ctx, "Failed to save project file to database: %v", err)
		return err
	}

	return nil
}
